package main;

import java.util.Scanner;

import pokerhands.PokerHandEvaluator;

/**
 * Main class that takes care of input and output.
 */
public class Main {
	private static Scanner scanner = new Scanner(System.in);
	private static PokerHandEvaluator evaluator = new PokerHandEvaluator();

	public static void main(String[] args) {
		String[] hand1;
		String[] hand2;

		// read in user input
		System.out.print("1st hand: ");
		hand1 = read5Cards();
		System.out.print("2nd hand: ");
		hand2 = read5Cards();

		System.out.println(evaluator.compareHands(hand1, hand2));
	}

	/**
	 * Reads in 5 cards and returns them as String array.
	 * 
	 * @return 5 Strings with the card descriptions (might be invalid cards)
	 */
	public static String[] read5Cards() {
		String[] cards = new String[5];

		for (int i = 0; i < 5; i++) {
			cards[i] = scanner.next();
		}

		return cards;
	}

}
