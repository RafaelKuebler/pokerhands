package pokerhands;

/**
 * Ranks for poker hands. Can be compared.
 */
public enum Rank implements Comparable<Rank> {
	HIGH_CARD("High Card"), PAIR("Pair"), TWO_PAIRS("Two Pairs"), THREE_OF_A_KIND("Three of a kind"), STRAIGHT(
			"Straight"), FLUSH("Flush"), FULL_HOUSE(
					"Full House"), FOUR_OF_A_KIND("Four of a kind"), STRAIGHT_FLUSH("Straight Flush");

	private String name;

	private Rank(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
