package pokerhands;

import exception.InvalidFormatException;

/**
 * A class representing a poker card. Possible values are 2 to 9. Possible
 * special values are stored with the following values as integers: T (ten) =
 * 10, J (jack) = 11, Q (1ueen) = 12, K (king) = 13, A (ace) = 14.
 * 
 * Possible suits are: C (clubs), D (diamonds), H (hearts), S (spades). Suits
 * are stored as chars.
 */
public class Card implements Comparable<Card> {
	private int value;
	private char suit;

	/**
	 * Constructs a new card from a String.
	 * 
	 * @param description
	 *            a String of length 2 with the suit followed by the value of
	 *            the card
	 * @throws InvalidFormatException
	 *             if the format of the description was not as specified
	 */
	public Card(String description) throws InvalidFormatException {
		if (description == null) {
			throw new InvalidFormatException("A card's description cannot be null.");
		}
		if (description.length() != 2) {
			throw new InvalidFormatException("A card's description must consists of 2 charcaters.");
		}

		suit = description.charAt(0);
		if (suit != 'C' && suit != 'D' && suit != 'H' && suit != 'S') {
			throw new InvalidFormatException("Invalid card suit '" + suit + "'.");
		}

		char valueChar = description.charAt(1);
		switch (valueChar) {
		case 'T':
			value = 10;
			break;
		case 'J':
			value = 11;
			break;
		case 'Q':
			value = 12;
			break;
		case 'K':
			value = 13;
			break;
		case 'A':
			value = 14;
			break;
		default:
			if (valueChar < '2' || valueChar > '9') {
				throw new InvalidFormatException("Invalid card value " + valueChar + ".");
			}
			value = Character.getNumericValue(description.charAt(1));
		}
	}

	@Override
	public int compareTo(Card otherCard) {
		// Cards with higher value are considered greater, creating an ascending
		// order by value.
		return Integer.valueOf(value).compareTo(Integer.valueOf(otherCard.getValue()));
	}

	public int getValue() {
		return value;
	}

	public char getSuit() {
		return suit;
	}

}
