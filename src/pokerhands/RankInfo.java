package pokerhands;

import java.util.ArrayList;

/**
 * The information to the rank of a poker hand. Stores the rank and additional
 * comparison values. Can be compared to other RankInfos.
 */
public class RankInfo implements Comparable<RankInfo> {
	private Rank rank;
	ArrayList<Integer> rankValuesList = new ArrayList<Integer>();

	public RankInfo(Rank rank, ArrayList<Integer> rankValuesList) {
		this.rank = rank;
		this.rankValuesList = rankValuesList;
	}

	/**
	 * Compares first by rank and then sequentially by the comparison values until a higher
	 * one is found.
	 */
	@Override
	public int compareTo(RankInfo otherRankInfo) {
		if (getRank().compareTo(otherRankInfo.getRank()) == 0) {
			for (int i = 0; i < rankValuesList.size(); i++) {
				ArrayList<Integer> otherValueList = otherRankInfo.getRankValuesList();

				if (i == otherValueList.size()) {
					break;
				} else if (rankValuesList.get(i) != otherValueList.get(i)) {
					return Integer.valueOf(rankValuesList.get(i)).compareTo(Integer.valueOf(otherValueList.get(i)));
				}
			}

			return 0;
		} else {
			return rank.compareTo(otherRankInfo.getRank());
		}
	}

	public Rank getRank() {
		return rank;
	}

	public ArrayList<Integer> getRankValuesList() {
		return rankValuesList;
	}
}
