package pokerhands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import exception.InvalidFormatException;

/**
 * Class representing a poker hand of 5 cards. Allows comparison of two hands by
 * rank.
 */
public class Hand implements Comparable<Hand> {
	private Card[] cards = new Card[5];
	private RankInfo rankInfo;

	/**
	 * Construct a new poker hand from a String array. Only hands containing 5
	 * cards are valid.
	 * 
	 * @param cardArrayAsString
	 *            array of Strings, one for each card
	 * @throws InvalidFormatException
	 *             if the number of cards was not equal to 5 or the cards
	 *             description was invalid
	 */
	public Hand(String[] cardArrayAsString) throws InvalidFormatException {
		if (cardArrayAsString.length != 5) {
			throw new InvalidFormatException("A hand must consist of 5 cards.");
		}

		for (int i = 0; i < 5; i++) {
			cards[i] = new Card(cardArrayAsString[i]);
		}

		Arrays.sort(cards);
		calculateRank();
	}

	/**
	 * Calculates and stores the RankInfo of the hand.
	 */
	private void calculateRank() {
		int valueOf5Consecutive = getValueOf5Consecutive();
		boolean allSuitsEqual = allSuitsEqual();
		CardsGroupedByValue cardGroups = getCardsGroupedByValue();
		Rank rank;

		ArrayList<Integer> rankValues = new ArrayList<>();
		for (int i = 4; i >= 0; i--) {
			rankValues.add(cards[i].getValue());
		}

		// redundancy for clarity
		if (valueOf5Consecutive > 0 && allSuitsEqual) {
			rankValues.add(0, valueOf5Consecutive);
			rank = Rank.STRAIGHT_FLUSH;
		} else if (cardGroups.hasGroupOfSize(4)) {
			rankValues.addAll(0, cardGroups.getValuesOfSize(4));
			rank = Rank.FOUR_OF_A_KIND;
		} else if (cardGroups.hasGroupOfSize(3) && cardGroups.hasGroupOfSize(2)) {
			rankValues.addAll(0, cardGroups.getValuesOfSize(3));
			rank = Rank.FULL_HOUSE;
		} else if (allSuitsEqual) {
			rank = Rank.FLUSH;
		} else if (valueOf5Consecutive > 0) {
			rankValues.add(0, valueOf5Consecutive);
			rank = Rank.STRAIGHT;
		} else if (cardGroups.hasGroupOfSize(3)) {
			rankValues.addAll(0, cardGroups.getValuesOfSize(3));
			rank = Rank.THREE_OF_A_KIND;
		} else if (cardGroups.hasGroupOfSize(2) && cardGroups.getValuesOfSize(2).size() == 2) {
			rankValues.removeAll(cardGroups.getValuesOfSize(2));
			rankValues.addAll(0, cardGroups.getValuesOfSize(2));
			rank = Rank.TWO_PAIRS;
		} else if (cardGroups.hasGroupOfSize(2)) { // Pair
			rankValues.removeAll(cardGroups.getValuesOfSize(2));
			rankValues.addAll(0, cardGroups.getValuesOfSize(2));
			rank = Rank.PAIR;
		} else {
			rank = Rank.HIGH_CARD;
		}

		rankInfo = new RankInfo(rank, rankValues);
	}

	/**
	 * Checks if all card suits are the same.
	 * 
	 * @return true if all suits are the same, false otherwise
	 */
	public boolean allSuitsEqual() {
		char suit = cards[0].getSuit();

		for (Card card : cards) {
			if (card.getSuit() != suit) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Groups the cards by value.
	 * 
	 * @return a CardsGroupedByValue instance with the grouped cards
	 */
	public CardsGroupedByValue getCardsGroupedByValue() {
		HashMap<Integer, Integer> appearingNumbers = new HashMap<>();

		for (Card card : cards) {
			int cardValue = card.getValue();
			if (appearingNumbers.containsKey(cardValue)) {
				appearingNumbers.put(cardValue, appearingNumbers.get(cardValue) + 1);
			} else {
				appearingNumbers.put(cardValue, 1);
			}
		}

		return new CardsGroupedByValue(appearingNumbers);
	}

	/**
	 * Gives back the highest value of the hand if all 5 card values are in
	 * consecutive order.
	 * 
	 * @return highest value of the hand if cards are in consecutive order, 0
	 *         otherwise
	 */
	public int getValueOf5Consecutive() {
		for (int i = 1; i <= 3; i++) {
			if (cards[i].getValue() != cards[i - 1].getValue() + 1) {
				return 0;
			}
		}
		// at this point the 4 first cards are in consecutive order

		// if the hand contains an ace it will be placed in the last position
		// because of the sorting
		// check if there could be a A 2 3 4 5 6 straight
		if (cards[4].getValue() == 14 && cards[0].getValue() == 2) {
			return 14;
		} else if (cards[4].getValue() == cards[3].getValue() + 1) {
			return cards[4].getValue();
		}

		return 0;
	}

	@Override
	public int compareTo(Hand otherHand) {
		// System.out.println("Rank 1: " + getRankInfo().getRank());
		// System.out.println("Rank 2: " + otherHand.getRankInfo().getRank());
		if (otherHand == null) {
			return -1;
		}
		return rankInfo.compareTo(otherHand.getRankInfo());
	}

	public Card[] getCards() {
		return cards;
	}

	public RankInfo getRankInfo() {
		return rankInfo;
	}
}
