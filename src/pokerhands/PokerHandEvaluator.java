package pokerhands;

import exception.InvalidFormatException;

/**
 * Evaluates both hands and gives back information on which hand wins the game
 * with what rank.
 */
public class PokerHandEvaluator {
	/**
	 * Compares the two hands by rank. Returns a message declaring the winning
	 * hand.
	 * 
	 * @param hand1String
	 *            the description of the first hand
	 * @param hand2String
	 *            the description of the second hand
	 * @return '1st hand wins!' or '2nd hand wins!', followed by the rank of the
	 *         winning hand
	 */
	public String compareHands(String[] hand1String, String[] hand2String) {
		Hand hand1;
		Hand hand2;
		try {
			hand1 = new Hand(hand1String);
			hand2 = new Hand(hand2String);
		} catch (InvalidFormatException exception) {
			return exception.getMessage();
		}

		int hand1VsHand2 = hand1.compareTo(hand2);

		if (hand1VsHand2 > 0) {
			return "1st hand wins! (" + hand1.getRankInfo().getRank() + ")";
		} else if (hand1VsHand2 < 0) {
			return "2nd hand wins! (" + hand2.getRankInfo().getRank() + ")";
		} else {
			return "Draw? (" + hand1.getRankInfo().getRank() + ")";
		}
	}
}
