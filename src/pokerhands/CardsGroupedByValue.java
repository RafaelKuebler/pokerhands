package pokerhands;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Stores groups of card values. Allows operations for groups of a certain size.
 */
public class CardsGroupedByValue {
	HashMap<Integer, Integer> valuesQuantity;

	/**
	 * Constructs new groups of cards with map of values to its frequency.
	 * 
	 * @param valuesQuantity
	 *            maps a value to the number of times it appears
	 */
	public CardsGroupedByValue(HashMap<Integer, Integer> valuesQuantity) {
		this.valuesQuantity = valuesQuantity;
	}

	/**
	 * Checks if there is a group of values of the indicated size.
	 * 
	 * @param size
	 *            the size of the group
	 * @return true if a group has the specified size, false otherwise
	 */
	public boolean hasGroupOfSize(int size) {
		if (valuesQuantity == null) {
			return false;
		}
		return valuesQuantity.containsValue(size);
	}

	/**
	 * Returns the values of a certain group size.
	 * 
	 * @param size
	 *            the size of the group
	 * @return all values that appear exactly size times
	 */
	public ArrayList<Integer> getValuesOfSize(int size) {
		ArrayList<Integer> valuesOfSize = new ArrayList<>();

		if (valuesQuantity != null) {
			for (Integer value : valuesQuantity.keySet()) {
				if (valuesQuantity.get(value) == size) {
					valuesOfSize.add(value);
				}
			}
		}

		return valuesOfSize;
	}
}
