To execute:
1. Navigate to .jar location (PokerHands.jar) in console
2. "java -jar PokerHands.jar"

Alternatively:
1. Import project to Eclipse
2. Run project from IDE
