package pokerhandtests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.After;
import org.junit.Test;

import pokerhands.Rank;
import pokerhands.RankInfo;

public class RankInfoTests {
	RankInfo rankInfo1;
	RankInfo rankInfo2;
	ArrayList<Integer> rankValueList1 = new ArrayList<>();
	ArrayList<Integer> rankValueList2 = new ArrayList<>();

	@After
	public void tearDown() throws Exception {
		rankValueList1.clear();
		rankValueList1.clear();
	}

	/**
	 * Checks if RankInfos with different ranks are compared independently of
	 * rank values.
	 */
	@Test
	public void testCompareToDifferentRank() {
		for (Rank rank1 : Rank.values()) {
			for (Rank rank2 : Rank.values()) {
				rankInfo1 = new RankInfo(rank1, rankValueList1);
				rankInfo2 = new RankInfo(rank2, rankValueList2);

				assertEquals(0, rankInfo1.compareTo(rankInfo1));
				assertEquals(0, rankInfo2.compareTo(rankInfo2));

				if (rank1.ordinal() > rank2.ordinal()) {
					assertTrue(rankInfo1.compareTo(rankInfo2) > 0);
					assertTrue(rankInfo2.compareTo(rankInfo1) < 0);
				} else if (rank1.ordinal() < rank2.ordinal()) {
					assertTrue(rankInfo1.compareTo(rankInfo2) < 0);
					assertTrue(rankInfo2.compareTo(rankInfo1) > 0);
				} else {
					assertEquals(0, rankInfo1.compareTo(rankInfo2));
					assertEquals(0, rankInfo2.compareTo(rankInfo1));
				}
			}
		}

	}

	/**
	 * Checks if RankInfos with the same ranks but no values are evaluated as
	 * equal.
	 */
	@Test
	public void testCompareToSameRankNoValues() {
		rankInfo1 = new RankInfo(Rank.values()[0], rankValueList1);
		rankInfo2 = new RankInfo(Rank.values()[0], rankValueList2);

		assertEquals(0, rankInfo1.compareTo(rankInfo1));
		assertEquals(0, rankInfo2.compareTo(rankInfo2));

	}

	/**
	 * Checks if RankInfos with the same ranks and 1 different value are
	 * evaluated by that different value.
	 */
	@Test
	public void testCompareToSameRank1Value() {
		rankValueList1.add(14);
		rankValueList2.add(2);

		rankInfo1 = new RankInfo(Rank.values()[0], rankValueList1);
		rankInfo2 = new RankInfo(Rank.values()[0], rankValueList2);

		assertTrue(rankInfo1.compareTo(rankInfo2) > 0);
		assertTrue(rankInfo2.compareTo(rankInfo1) < 0);

	}

	/**
	 * Checks if RankInfos with the same ranks, many equal and 1 different value
	 * are evaluated by that different value.
	 */
	@Test
	public void testCompareToSameRankMoreValues() {
		ArrayList<Integer> rankValueList1 = new ArrayList<>();
		ArrayList<Integer> rankValueList2 = new ArrayList<>();
		rankValueList1.addAll(Arrays.asList(2, 2, 3));
		rankValueList2.addAll(Arrays.asList(2, 2, 2));

		rankInfo1 = new RankInfo(Rank.values()[0], rankValueList1);
		rankInfo2 = new RankInfo(Rank.values()[0], rankValueList2);

		assertTrue(rankInfo1.compareTo(rankInfo2) > 0);
		assertTrue(rankInfo2.compareTo(rankInfo1) < 0);

	}
}
