package pokerhandtests;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.junit.After;
import org.junit.Test;

import pokerhands.CardsGroupedByValue;

public class CardsGroupedByValueTest {
	HashMap<Integer, Integer> valueQuantity = new HashMap<>();
	CardsGroupedByValue groupedCards;

	@After
	public void tearDown() {
		valueQuantity.clear();
	}

	/**
	 * Checks if false is returned if asking for a certain group size and no
	 * groups are available.
	 */
	@Test
	public void testHasGroupOfSizeNoGroups() {
		groupedCards = new CardsGroupedByValue(valueQuantity);

		for (int i = 1; i <= 5; i++) {
			assertFalse(groupedCards.hasGroupOfSize(i));
		}
	}

	/**
	 * Checks if group size testing defines group sizes correctly for 1 example
	 * value.
	 */
	@Test
	public void testHasGroupOfSize1Value() {
		valueQuantity.put(2, 5);
		groupedCards = new CardsGroupedByValue(valueQuantity);
		assertTrue(groupedCards.hasGroupOfSize(5));
		assertFalse(groupedCards.hasGroupOfSize(1));
	}

	/**
	 * Checks if group size testing defines group sizes correctly for 3 example
	 * value.
	 */
	@Test
	public void testHasGroupOfSize3Values() {
		valueQuantity.put(1, 1);
		valueQuantity.put(14, 2);
		valueQuantity.put(10, 2);
		groupedCards = new CardsGroupedByValue(valueQuantity);
		assertTrue(groupedCards.hasGroupOfSize(2));
		assertTrue(groupedCards.hasGroupOfSize(1));
		assertFalse(groupedCards.hasGroupOfSize(3));
	}

	/**
	 * Checks if correct values of a certain size are returned when asking for
	 * an example Full House.
	 */
	@Test
	public void testGetValuesOfSizeFullHouse() {
		valueQuantity.put(3, 3);
		valueQuantity.put(5, 2);
		groupedCards = new CardsGroupedByValue(valueQuantity);
		assertTrue(groupedCards.getValuesOfSize(3).size() == 1);
		assertTrue(groupedCards.getValuesOfSize(3).contains(3));
		assertTrue(groupedCards.getValuesOfSize(2).size() == 1);
		assertTrue(groupedCards.getValuesOfSize(2).contains(5));
	}

	/**
	 * Checks if correct values of a certain size are returned when asking for
	 * an example Two Pair.
	 */
	@Test
	public void testGetValuesOfSizeTwoPair() {
		valueQuantity.put(1, 1);
		valueQuantity.put(14, 2);
		valueQuantity.put(10, 2);
		groupedCards = new CardsGroupedByValue(valueQuantity);
		assertTrue(groupedCards.getValuesOfSize(1).size() == 1);
		assertTrue(groupedCards.getValuesOfSize(1).contains(1));
		assertTrue(groupedCards.getValuesOfSize(2).size() == 2);
		assertTrue(groupedCards.getValuesOfSize(2).contains(10));
		assertTrue(groupedCards.getValuesOfSize(2).contains(14));
	}

}
