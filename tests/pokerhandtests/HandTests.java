package pokerhandtests;

import static org.junit.Assert.*;

import org.junit.Test;

import exception.InvalidFormatException;
import pokerhands.Card;
import pokerhands.Hand;

public class HandTests {
	private Hand hand;
	private final String[] straightFlush = new String[] { "S4", "S3", "S5", "S7", "S6" };
	private final String[] fourOfAKind = new String[] { "D5", "H5", "S5", "D7", "C5" };
	private final String[] fullHouse = new String[] { "CQ", "SQ", "S2", "HQ", "H2" };
	private final String[] flush = new String[] { "D9", "DK", "D6", "DQ", "D3" };
	private final String[] straight = new String[] { "H4", "S3", "H5", "S7", "D6" };
	private final String[] threeOfAKind = new String[] { "H4", "HA", "SA", "S2", "DA" };
	private final String[] twoPairs = new String[] { "CJ", "S4", "SJ", "H8", "D8" };
	private final String[] pair = new String[] { "S3", "S7", "DQ", "HQ", "C6" };
	private final String[] highCard = new String[] { "S4", "HJ", "D7", "CK", "S9" };
	private final String[] highCard2 = new String[] { "S3", "CJ", "S7", "HK", "H9" };

	/**
	 * Checks if after creating a hand with a description the cards exist.
	 */
	@Test
	public void storedCardsExist() {
		try {
			hand = new Hand(straightFlush);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}

		assertNotNull(hand.getCards());

		for (Card card : hand.getCards()) {
			assertNotNull(card);
		}
	}

	/**
	 * Tests if a hand with less than 5 cards is rejected.
	 */
	@Test
	public void testInvalidCard() {
		String[] invalidHand = new String[] { "C4", "S3", "S5", "S7" };

		try {
			hand = new Hand(invalidHand);
		} catch (InvalidFormatException exception) {
			return;
		}
		fail();
	}

	/**
	 * Checks if a hand where all the cards have the same suit is correctly
	 * detected as false in a high card.
	 */
	@Test
	public void testAllSuitsEqualHighCard() {
		try {
			hand = new Hand(highCard);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertFalse(hand.allSuitsEqual());

	}

	/**
	 * Checks if a hand where all the cards have the same suit is correctly
	 * detected as true in a Straight Flush.
	 */
	@Test
	public void testAllSuitsEqualStraighFlush() {
		try {
			hand = new Hand(flush);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertTrue(hand.allSuitsEqual());
	}

	/**
	 * Checks if a hand where all cards are in consecutive order is detected
	 * correctly with a Full House and returns highest value 0.
	 */
	@Test
	public void testGetValueOf5ConsecutiveFullHouse() {
		try {
			hand = new Hand(fullHouse);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertEquals(0, hand.getValueOf5Consecutive());
	}

	/**
	 * Checks if a hand where all cards are in consecutive order is detected
	 * correctly with a Straight and returns the highest card value.
	 */
	@Test
	public void testGetValueOf5ConsecutiveStraight() {
		try {
			hand = new Hand(straight);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertEquals(7, hand.getValueOf5Consecutive());
	}

	/**
	 * Checks if cards are grouped correctly in a Pair.
	 */
	@Test
	public void testGetCardsGroupedByValuePair() {
		try {
			hand = new Hand(pair);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertTrue(hand.getCardsGroupedByValue().hasGroupOfSize(1));
		assertTrue(hand.getCardsGroupedByValue().hasGroupOfSize(2));
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(1).size() == 3);
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(2).size() == 1);
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(2).contains(12));
	}

	/**
	 * Checks if cards are grouped correctly in a Three of a kind.
	 */
	@Test
	public void testGetCardsGroupedByValueThreeOfAKind() {
		try {
			hand = new Hand(threeOfAKind);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertTrue(hand.getCardsGroupedByValue().hasGroupOfSize(1));
		assertTrue(hand.getCardsGroupedByValue().hasGroupOfSize(3));
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(1).size() == 2);
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(3).size() == 1);
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(1).contains(2));
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(1).contains(4));
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(3).contains(14));

	}

	/**
	 * Checks if cards are grouped correctly in a Four of a kind.
	 */
	public void testGetCardsGroupedByValueFourOfAKind() {
		try {
			hand = new Hand(fourOfAKind);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertTrue(hand.getCardsGroupedByValue().hasGroupOfSize(1));
		assertTrue(hand.getCardsGroupedByValue().hasGroupOfSize(4));
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(1).size() == 1);
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(4).size() == 1);
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(1).contains(7));
		assertTrue(hand.getCardsGroupedByValue().getValuesOfSize(4).contains(5));
	}

	/**
	 * Compares a few combinations of example hands.
	 */
	@Test
	public void testCompareTo() {
		Hand hand2 = null;

		try {
			hand = new Hand(straightFlush);
			hand2 = new Hand(straight);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertTrue(hand.compareTo(hand2) > 0);
		assertTrue(hand2.compareTo(hand) < 0);

		try {
			hand = new Hand(straightFlush);
			hand2 = new Hand(flush);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertTrue(hand.compareTo(hand2) > 0);
		assertTrue(hand2.compareTo(hand) < 0);

		try {
			hand = new Hand(twoPairs);
			hand2 = new Hand(threeOfAKind);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertTrue(hand.compareTo(hand2) < 0);
		assertTrue(hand2.compareTo(hand) > 0);

		try {
			hand = new Hand(highCard);
			hand2 = new Hand(highCard);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertTrue(hand.compareTo(hand2) == 0);

		try {
			hand = new Hand(highCard);
			hand2 = new Hand(highCard2);
		} catch (InvalidFormatException exception) {
			fail("Exception: " + exception.getMessage());
		}
		assertTrue(hand.compareTo(hand2) > 0);
		assertTrue(hand2.compareTo(hand) < 0);
	}
}
