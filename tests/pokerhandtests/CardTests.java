package pokerhandtests;

import static org.junit.Assert.*;

import org.junit.Test;

import exception.InvalidFormatException;
import pokerhands.Card;

public class CardTests {
	Card card;

	/**
	 * Tests if the Card class converts and stores the values correctly checking
	 * every combination of suits and values.
	 */
	@Test
	public void testAllSuitsAndValues() {
		// all possible suits
		char[] suits = new char[] { 'C', 'D', 'H', 'S' };
		// all possible values
		for (int value = 2; value <= 14; value++) {
			for (int i = 0; i < 4; i++) {
				char valueChar;
				char suitChar = suits[i];

				switch (value) {
				// convert special characters
				case 10:
					valueChar = 'T';
					break;
				case 11:
					valueChar = 'J';
					break;
				case 12:
					valueChar = 'Q';
					break;
				case 13:
					valueChar = 'K';
					break;
				case 14:
					valueChar = 'A';
					break;
				default:
					valueChar = (char) (value + '0');
					break;
				}

				try {
					card = new Card(new String(new char[] { suitChar, valueChar }));
				} catch (InvalidFormatException exception) {
					fail("Exception: " + exception.getMessage());
				}

				assertTrue(card.getValue() == value);
				assertTrue(card.getSuit() == suits[i]);
			}
		}
	}

	/**
	 * Tests if a card with a wrong suit is rejected.
	 */
	@Test
	public void testInvalidSuit() {
		try {
			card = new Card("P5");
		} catch (InvalidFormatException exception) {
			return;
		}
		fail();
	}

	/**
	 * Tests if a card with a wrong value is rejected.
	 */
	@Test
	public void testInvalidValue() {
		try {
			card = new Card("DD");
		} catch (InvalidFormatException exception) {
			return;
		}
		fail();
	}
}
